unit Figure;

//{$mode objfpc}{$H+}

interface

uses
  Classes, Windows, SysUtils, Graphics;

type
  TFigure = class
  private
    created: TDateTime;
    function CurrentAngle(): real;

  public
    Pt1: TPoint;
    Pt2: TPoint;
    PtCenter: TPoint;
    RotationVelocity: real;
    constructor Create; overload;
    constructor Create(_pt1, _pt2, _pt_center: TPoint;
      _rotation_velocity: real); overload;
    procedure Draw(canvas: TCanvas);

  end;


implementation

function Rotate(pt: TPoint; center: TPoint; a: real): TPoint;
  // ������� �������� ����� ������������ �������� ����� �� �������� ����.
var
  x, y, xc, yc: real;
begin
  x := pt.X;
  y := pt.Y;
  xc := center.X;
  yc := center.Y;
  Result.X := Round(xc + (x - xc) * cos(a) - (y - yc) * sin(a));
  Result.Y := Round(yc + (y - yc) * cos(a) + (x - xc) * sin(a));
end;

constructor TFigure.Create();
begin
  self.created := Now;
end;

constructor TFigure.Create(_pt1, _pt2, _pt_center: TPoint; _rotation_velocity: real);
begin
  Create;
  self.Pt1 := _pt1;
  self.Pt2 := _pt2;
  self.PtCenter := _pt_center;
  self.RotationVelocity := _rotation_velocity;
end;

function TFigure.CurrentAngle(): real;
begin
  Result := (Now - self.created) * 86400 * RotationVelocity * 2 * pi;
end;

procedure TFigure.Draw(canvas: TCanvas);
var
  a: real;  // ������� ���� ��������
  rotated_pt1, rotated_pt2: TPoint;  // ������� ���������� ������ �������
begin
  // ��������� ��������
  a := CurrentAngle();
  rotated_pt1 := Rotate(Pt1, PtCenter, a);
  rotated_pt2 := Rotate(Pt2, PtCenter, a);

  // ������
  with canvas do
  begin
    // �������
    Pen.Color := clRed;
    Pen.Style := psSolid;
    Pen.Width := 3;
    MoveTo(rotated_pt1.X, rotated_pt1.Y);
    LineTo(rotated_pt2.X, rotated_pt2.Y);

    // ���������� ����� � ������ ��� �����������
    Pen.Color := clRed;
    Pen.Style := psDash;
    Pen.Width := 1;
    MoveTo(rotated_pt1.X, rotated_pt1.Y);
    LineTo(PtCenter.X, PtCenter.Y);
    LineTo(rotated_pt2.X, rotated_pt2.Y);
  end;
end;

end.



