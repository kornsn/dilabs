unit Unit1;

//{$mode objfpc}{$H+}

interface

uses
  Classes,
  Windows,
  SysUtils,
  Forms,
  Controls,
  Graphics,
  Dialogs,
  ExtCtrls,
  StdCtrls,
  Figure;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Timer1: TTimer;
    x1_edit: TEdit;
    y1_edit: TEdit;
    x2_edit: TEdit;
    y2_edit: TEdit;
    xc_edit: TEdit;
    yc_edit: TEdit;
    v_edit: TEdit;
    Image1: TImage;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure StartClick(Sender: TObject);
    procedure SetVariables();
    procedure Timer1Timer(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form1: TForm1;
  Line: TFigure;

implementation

{$R *.dfm}

{ TForm1 }
procedure ClearCanvas();
begin
  with Form1.Image1.Canvas do FillRect(ClipRect);
end;

procedure TForm1.SetVariables();
var
  pt1, pt2, center: TPoint;
  v: Real;
// �������� �������� �� ��������� �����
begin
  pt1 := Point(StrToInt(x1_edit.Text), StrToInt(y1_edit.Text));
  pt2 := Point(StrToInt(x2_edit.Text), StrToInt(y2_edit.Text));
  center := Point(StrToInt(xc_edit.Text), StrToInt(yc_edit.Text));
  v := StrToFloat(v_edit.Text);
  Line := TFigure.Create(pt1, pt2, center, v);
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  ClearCanvas();
  Line.Draw(Image1.Canvas);
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  // ����� �������� ����� ����� �������� ������, ����� ���� �������
  ClearCanvas();
end;

procedure TForm1.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then Close;  // ������� ��� ������� ESC
end;


procedure TForm1.StartClick(Sender: TObject);
// ������ ����
begin
  SetVariables();
  Timer1.Enabled:=True;
end;

end.

