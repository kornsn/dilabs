object Form1: TForm1
  Left = 219
  Top = 0
  BorderStyle = bsSingle
  Caption = #1051#1072#1073#1086#1088#1072#1090#1086#1088#1085#1072#1103' 2 '#1061#1091#1076#1103#1082#1086#1074#1072' '#1044'.'#1053'.'
  ClientHeight = 425
  ClientWidth = 478
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = True
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = 152
    Top = 8
    Width = 305
    Height = 392
  end
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 12
    Height = 13
    Caption = 'x1'
    Color = clBtnFace
    ParentColor = False
  end
  object Label2: TLabel
    Left = 8
    Top = 56
    Width = 12
    Height = 13
    Caption = 'y1'
    Color = clBtnFace
    ParentColor = False
  end
  object Label3: TLabel
    Left = 8
    Top = 104
    Width = 12
    Height = 13
    Caption = 'x2'
    Color = clBtnFace
    ParentColor = False
  end
  object Label4: TLabel
    Left = 8
    Top = 152
    Width = 12
    Height = 13
    Caption = 'y2'
    Color = clBtnFace
    ParentColor = False
  end
  object Label5: TLabel
    Left = 8
    Top = 200
    Width = 40
    Height = 13
    Caption = 'x center'
    Color = clBtnFace
    ParentColor = False
  end
  object Label6: TLabel
    Left = 8
    Top = 248
    Width = 40
    Height = 13
    Caption = 'y center'
    Color = clBtnFace
    ParentColor = False
  end
  object Label7: TLabel
    Left = 8
    Top = 296
    Width = 88
    Height = 13
    Caption = #1057#1082#1086#1088#1086#1089#1090#1100', '#1086#1073'/'#1089#1077#1082
    Color = clBtnFace
    ParentColor = False
  end
  object x1_edit: TEdit
    Left = 8
    Top = 24
    Width = 80
    Height = 21
    TabOrder = 0
    Text = '10'
  end
  object y1_edit: TEdit
    Left = 8
    Top = 72
    Width = 80
    Height = 21
    TabOrder = 1
    Text = '20'
  end
  object x2_edit: TEdit
    Left = 8
    Top = 120
    Width = 80
    Height = 21
    TabOrder = 2
    Text = '100'
  end
  object y2_edit: TEdit
    Left = 8
    Top = 168
    Width = 80
    Height = 21
    TabOrder = 3
    Text = '40'
  end
  object xc_edit: TEdit
    Left = 8
    Top = 216
    Width = 80
    Height = 21
    TabOrder = 4
    Text = '40'
  end
  object yc_edit: TEdit
    Left = 8
    Top = 264
    Width = 80
    Height = 21
    TabOrder = 5
    Text = '60'
  end
  object v_edit: TEdit
    Left = 8
    Top = 312
    Width = 80
    Height = 21
    TabOrder = 6
    Text = '1'
  end
  object Button1: TButton
    Left = 13
    Top = 352
    Width = 75
    Height = 25
    Caption = #1057#1090#1072#1088#1090
    TabOrder = 7
    OnClick = StartClick
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 30
    OnTimer = Timer1Timer
    Top = 397
  end
end
